


db.users.insertMany([
    {
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
    phone: "87654321",
    email: "janedoe@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "HR"
    },
    
    {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
   phone: "87654321",
   email: "stephenhawking@gmail.com"
    },
    courses: [ "Python", "React", "PHP" ],
    department: "HR"
    },
    
    {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
   phone: "87654321",
   email: "neilarmstrong@gmail.com"
    },
    courses: [ "React", "Laravel", "Sass" ],
    department: "HR"
    }
   ]);


db.users.insertOne({
    firstName:'Bill',
    lastName: 'Gates',
    age: 65,
    contact: {
        phone: '12345678',
        email: 'bill@gmail.com'
    },
    courses: ['PHP', 'Laravel', 'HTML'],
    department: 'Operations'

})




// Query Operator


// Comparision Query Operators


// $gt  = greater than
db.users.find( {age:  {$gt:65} });

// $gte = greater thatn or equal
db.users.find( { age: {$gte: 65}});

// $lt = less than
db.users.find( {age: {$lt:65}});

// $lte = less than or equal
db.users.find( { age: {$lte: 65}});

// $ne = not equal
db.users.find( { age: {$ne: 82}});


// $in = Allows us to find documents with specific match criteria of one field using different values.
db.users.find( { lastName: {$in: ['Hawking', 'Doe']}});

db.users.find( { courses: { $in: ['HTML', 'React']}});


// $or Operator

// finding firstName = Neil or age = 21 
db.users.find(
    {
        $or: [
            { firstName: 'Neil'},
            {age: 21}
        ]
    }
)


// combination with $or and $gt
db.users.find(
    {
        $or: [
            { firstName: 'Jane'},
            {age:{$gt: 30}}
        ]
    }
)

// $and Operator

db.users.find( {
    $and: [
        {age: {$ne:82}},
        {age: {$ne:76}}
    ]
})


//Mini quest
   // Look for the users that have the courses "Laravel" or "React" and whose age is less than 80 years old.

        // Take a screenshot of result and send it to the batch hangouts.

        // Expected Result: Stephen Hawking and Bill Gates


db.users.find({
    $and: [
        {courses: {$in: ['Laravel', 'React']}},
        {age: {$lt: 80}}
    ]
})




//Field Projection
// To help with readability of the values returned, we can include/exclude fields from the retrieve results.


// Inclusion
/**
 * - Allows us to include or add specific fields only when retrieving documents.
 * - The value provided is one to denote that thet the field being included.
 * 
 * syntax:
 *  db.users.find( {criteria}, {field: 1})
 */


db.users.find(
     {firstName: 'Jane'},

     // this will return the specific field you want all equal to 1
     {firstName: 1, contact: 1, lastName:1}

     )


// Exclusion
/**
 * - Allows us to exclude specific fields only when retrieving documents.
 * - The value provided is 0 to denote that thet the field being included.
 * 
 * syntax:
 *  db.users.find( {criteria}, {field: 0})
 */

db.users.find({firstName: 'Jane'}, {_id:0, contact: 0, department: 0} );


//Mini activity

// Supressing the ID Field
     // - When using field projection, field inclusion and exclusion may not be used at the same time.
     // - Excluding the '_id' field is the only exception to this rule.
     // syntax: db.collectionName.find({criteria}, {field:1, _id:0})


db.users.find( 
    { lastName: 'Doe'},
    
    {
        firstName: 1,
        lastName: 1,
        contact:1,
        _id: 0
    }

)//Return a specific field in Embedded Documents.
db.users.find({firstName: 'Jane'},
 {
    firstName: 1, 
    lastName:1, 
    'contact.phone':1  //Included only the phone in the contact
}
)

// This will exclude the phone in the contact. email is still there
db.users.find( {firstName: 'Jane'}, {'contact.phone':0});



// $slice

db.users.find( {firstName: 'Jane'},
    {
        courses: {$slice: 2}
    }
)

// return with the number of index and the count of item. 1 is the index to start and 2 is the count of the item.
db.users.find( {firstName: 'Jane'},
    {
        courses: {$slice: [1,2]}
    }
)


// $regex operator
/** 
 *  - Allows us to find documents that match a specific string pattern using 'regualr expression' or regex.
 * 
 * syntac:
 * db.collectionName.find({criteria: {field: $regex: 'patter' $options: 'optionValue' }})
 * 
 */

//case sensitive query
db.users.find({firstName:{$regex:'ne'}})

// case insensitive query
db.users.find({firstName:{$regex:'ne',$options:'$i'} })